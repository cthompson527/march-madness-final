# -- Base
FROM node:11.10.0-alpine AS base
ENV NPM_CONFIG_LOGLEVEL info
WORKDIR /home/node/api
COPY ./package.json ./
COPY ./package-lock.json ./

# -- Dependencies
FROM base as dependencies
RUN npm install

FROM dependencies as build
COPY ./tsconfig.json ./
COPY ./src ./src
RUN npx tsc -p tsconfig.json

# ---- Release ----
FROM node:11.10.0-alpine AS release
# copy production node_modules
WORKDIR /home/node/api
COPY --from=build /home/node/api/node_modules ./node_modules
COPY --from=build /home/node/api/lib ./lib

EXPOSE 4000
CMD ["node", "lib/server.js"]

import DotEnv from 'dotenv';
import sequelize from 'sequelize';

DotEnv.config();
const dbName = process.env.POSTGRES_DB || 'march-madness';
const dbUser = process.env.POSTGRES_USER || '';
const dbPassword = process.env.POSTGRES_PASSWORD || '';
const host = process.env.POSTGRES_HOST || 'localhost';
const port = parseInt(process.env.POSTGRES_PORT || '5432', 10);

const Op = sequelize.Op;
const operatorsAliases = {
  $lt: Op.lt,
  $eq: Op.eq,
  $ne: Op.ne,
};

export default new sequelize(dbName, dbUser, dbPassword, {
  define: {
    underscored: true,
    underscoredAll: true,
  },
  dialect: 'postgres',
  host,
  port,
  operatorsAliases,
  logging: process.env.NODE_ENV !== 'prod',
});

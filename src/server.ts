import express from 'express';
import bodyParser from 'body-parser';
import compression from 'compression';
import 'source-map-support/register';
import { getGames, postGames } from './handlers/gamesHandlers';
import { setupDb } from './connectors';
import { postUser, verifyToken, login } from './handlers/userHandler';
import { getTeams } from './handlers/teamsHandler';
import { getSelections, postSelections } from './handlers/selectionsHandler';
import { getScores } from './handlers/userScores';

setupDb();

const host = process.env.SERVER_HOST || 'localhost';
const port = parseInt(process.env.SERVER_PORT || '4000', 10);

const app = express();
app.use(bodyParser());
app.use(compression());
app.use(
  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
    res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-Width, Content-Type, Authorization, Accept',
    );
    res.header('Access-Control-Expose-Headers', 'Authorization');
    if (req.method === 'OPTIONS') {
      res.sendStatus(200);
    } else {
      next();
    }
  },
);

app.use((req, res, next) => {
  if (req.originalUrl === '/login' || req.originalUrl === '/register') {
    next();
    return;
  }
  const token = req.headers.authorization;
  if (!token) {
    res.status(401).send('user is unauthorized');
    return;
  }
  const t = token.substring(7);
  try {
    const { uuid: user_id, admin } = verifyToken(t);
    req.headers.user_id = user_id;
    req.headers.admin = `${admin}`;
  } catch (err) {
    res.status(401).send(err.toString());
    return;
  }
  next();
});

app.get('/');
app.get('/selections/', getSelections);
app.get('/selections/:userid', getSelections);
app.get('/teams', getTeams);
app.get('/games', getGames);
app.get('/scores', getScores);
app.post('/register', postUser);
app.post('/login', login);
app.post('/selections', postSelections);
app.post('/games', postGames);

app.listen(port, host, (err: Error) => {
  if (err) {
    return console.error('cannot listen', err);
  }

  console.log(`server is listening on port ${4000}`);
});

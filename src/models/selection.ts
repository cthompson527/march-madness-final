import { DataTypes, Sequelize } from 'sequelize';

export default (sequelize: Sequelize, dataTypes: DataTypes) =>
  sequelize.define(
    'selection',
    {
      id: {
        type: dataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      user_id: {
        type: dataTypes.UUID,
        allowNull: false,
        unique: 'compositeIndex',
      },
      game_id: {
        type: dataTypes.STRING,
        allowNull: false,
        unique: 'compositeIndex',
      },
      competitor_id: {
        type: dataTypes.INTEGER,
        allowNull: false,
      },
    },
  );
import { DataTypes, Sequelize } from 'sequelize';

export default (sequelize: Sequelize, dataTypes: DataTypes) =>
  sequelize.define(
    'user',
    {
      uuid: {
        type: dataTypes.UUID,
        defaultValue: dataTypes.UUIDV4,
        primaryKey: true,
      },
      email: {
        type: dataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      first_name: {
        type: dataTypes.STRING,
        allowNull: false,
      },
      last_name: {
        type: dataTypes.STRING,
        allowNull: false,
      },
      admin: {
        type: dataTypes.BOOLEAN,
        allowNull: false,
      },
    },
    {},
  );

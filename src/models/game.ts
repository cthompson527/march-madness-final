import { DataTypes, Sequelize } from 'sequelize';

export default (sequelize: Sequelize, dataTypes: DataTypes) =>
  sequelize.define(
    'game',
    {
      id: {
        type: dataTypes.STRING,
        allowNull: false,
        primaryKey: true,
      },
      seeds: {
        type: dataTypes.STRING,
        allowNull: false,
      },
      away_team: {
        type: dataTypes.INTEGER,
        allowNull: true,
      },
      home_team: {
        type: dataTypes.INTEGER,
        allowNull: true,
      },
      away_score: {
        type: dataTypes.INTEGER,
        allowNull: true,
      },
      home_score: {
        type: dataTypes.INTEGER,
        allowNull: true,
      },
    },
    {
      timestamps: false,
    },
  );

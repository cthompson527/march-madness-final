import { DataTypes, Sequelize } from 'sequelize';

export default (sequelize: Sequelize, dataTypes: DataTypes) =>
  sequelize.define(
    'competitor',
    {
      id: {
        type: dataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      team_id: {
        type: dataTypes.INTEGER,
      },
      bracket: {
        type: dataTypes.STRING,
        allowNull: false,
      },
      seed: {
        type: dataTypes.INTEGER,
        allowNull: false,
      }
    },
    {
      timestamps: false,
    },
  );

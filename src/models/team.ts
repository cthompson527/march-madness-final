import { DataTypes, Sequelize } from 'sequelize';

export default (sequelize: Sequelize, dataTypes: DataTypes) =>
  sequelize.define(
    'team',
    {
      id: {
        type: dataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: dataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      short_name: {
        type: dataTypes.STRING,
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: false,
    },
  );

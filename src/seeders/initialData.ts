import { competitors, teams } from '../connectors';

const seedTeamsData = [
  {
    name: 'Texas A&M',
    short_name: 'TAMU',
  },
  {
    name: 'North Carolina',
    short_name: 'UNC',
  },
  {
    name: 'Virginia',
    short_name: 'UVA',
  },
  {
    name: 'Cincinnati',
  },
  {
    name: 'Tennessee',
    short_name: 'TENN',
  },
  {
    name: 'Arizona',
    short_name: 'UA',
  },
  {
    name: 'Kentucky',
    short_name: 'KENT',
  },
  {
    name: 'Miami (FL)',
  },
  {
    name: 'Nevada',
  },
  {
    name: 'Creighton',
  },
  {
    name: 'Kansas State',
  },
  {
    name: 'Texas',
    short_name: 'UT',
  },
  {
    name: 'Loyola–Chicago',
  },
  {
    name: 'Davidson',
  },
  {
    name: 'Buffalo',
  },
  {
    name: 'Wright State',
  },
  {
    name: 'Georgia State',
  },
  {
    name: 'UMBC',
  },
  {
    name: 'Xavier',
  },
  {
    name: 'Michigan',
  },
  {
    name: 'Gonzaga',
  },
  {
    name: 'Ohio State',
    short_name: 'OSU',
  },
  {
    name: 'Houston',
    short_name: 'UH',
  },
  {
    name: 'Missouri',
    short_name: 'MISS',
  },
  {
    name: 'Florida State',
    short_name: 'FSU',
  },
  {
    name: 'Providence',
  },
  {
    name: 'San Diego State',
    short_name: 'SDST',
  },
  {
    name: 'South Dakota State',
    short_name: 'SoDST',
  },
  {
    name: 'Montana',
  },
  {
    name: 'Lipscomb',
  },
  {
    name: 'Texas Southern',
  },
  {
    name: 'Villanova',
  },
  {
    name: 'Purdue',
  },
  {
    name: 'Texas Tech',
    short_name: 'TTU',
  },
  {
    name: 'Wichita State',
    short_name: 'WST',
  },
  {
    name: 'West Virginia',
    short_name: 'WVU',
  },
  {
    name: 'Florida',
    short_name: 'UF',
  },
  {
    name: 'Arkansas',
    short_name: 'ARK',
  },
  {
    name: 'Virginia Tech',
    short_name: 'VTU',
  },
  {
    name: 'Alabama',
    short_name: 'BAMA',
  },
  {
    name: 'Butler',
  },
  {
    name: 'St. Bonaventure',
  },
  {
    name: 'Murray State',
  },
  {
    name: 'Marshall',
  },
  {
    name: 'Stephen F. Austin',
    short_name: 'SFA',
  },
  {
    name: 'Cal State Fullerton',
    short_name: 'CSF',
  },
  {
    name: 'Radford',
  },
  {
    name: 'Kansas',
    short_name: 'UK',
  },
  {
    name: 'Duke',
  },
  {
    name: 'Michigan State',
    short_name: 'MSU',
  },
  {
    name: 'Auburn',
  },
  {
    name: 'Clemson',
  },
  {
    name: 'TCU',
  },
  {
    name: 'Rhode Island',
  },
  {
    name: 'Seton Hall',
  },
  {
    name: 'NC State',
    short_name: 'NCST',
  },
  {
    name: 'Oklahoma',
    short_name: 'OU',
  },
  {
    name: 'Syracuse',
  },
  {
    name: 'New Mexico State',
    short_name: 'NMST',
  },
  {
    name: 'College of Charleston',
    short_name: 'CC',
  },
  {
    name: 'Bucknell',
  },
  {
    name: 'Iona',
  },
  {
    name: 'Penn',
  },
  {
    name: 'UNC Greensboro',
  },
  {
    name: 'NCC/NDAKST',
  },
  {
    name: 'VCU',
  },
  {
    name: 'UCF',
  },
  {
    name: 'Mississippi St',
  },
  {
    name: 'Liberty',
  },
  {
    name: 'Saint Louis',
  },
  {
    name: 'Maryland',
  },
  {
    name: 'Belmont/Temple',
  },
  {
    name: 'LSU',
  },
  {
    name: 'Yale',
  },
  {
    name: 'Louisville',
  },
  {
    name: 'Minnesota',
  },
  {
    name: 'Bradley',
  },
  {
    name: 'FDickenson/PrView',
  },
  {
    name: 'Baylor',
  },
  {
    name: 'Northern Kentucky',
  },
  {
    name: 'ASU/StJohns',
  },
  {
    name: 'Vermont',
  },
  {
    name: 'Marquette',
  },
  {
    name: 'Colgate',
  },
  {
    name: 'Gardner-Webb',
  },
  {
    name: 'Ole Miss',
  },
  {
    name: 'Wisconsin',
  },
  {
    name: 'Oregon',
  },
  {
    name: 'UC Irvine',
  },
  {
    name: 'Saint Mary',
  },
  {
    name: 'Old Dominion',
  },
  {
    name: 'Iowa',
  },
  {
    name: 'Abilene Christian',
  },
  {
    name: 'Wofford',
  },
  {
    name: 'Iowa State',
  },
  {
    name: 'Northeastern',
  },
  {
    name: 'Washington',
  },
  {
    name: 'Utah State',
  },
];

const seedCompetitorsData = [
  {
    team_name: 'Duke',
    seed: 1,
    bracket: 'East',
  },
  {
    team_name: 'NCC/NDAKST',
    seed: 16,
    bracket: 'East',
  },
  {
    team_name: 'VCU',
    seed: 8,
    bracket: 'East',
  },
  {
    team_name: 'UCF',
    seed: 9,
    bracket: 'East',
  },
  {
    team_name: 'Mississippi St',
    seed: 5,
    bracket: 'East',
  },
  {
    team_name: 'Liberty',
    seed: 12,
    bracket: 'East',
  },
  {
    team_name: 'Virginia Tech',
    seed: 4,
    bracket: 'East',
  },
  {
    team_name: 'Virginia Tech',
    seed: 4,
    bracket: 'East',
  },
  {
    team_name: 'Saint Louis',
    seed: 13,
    bracket: 'East',
  },
  {
    team_name: 'Maryland',
    seed: 6,
    bracket: 'East',
  },
  {
    team_name: 'Belmont/Temple',
    seed: 11,
    bracket: 'East',
  },
  {
    team_name: 'LSU',
    seed: 3,
    bracket: 'East',
  },
  {
    team_name: 'Yale',
    seed: 14,
    bracket: 'East',
  },
  {
    team_name: 'Louisville',
    seed: 7,
    bracket: 'East',
  },
  {
    team_name: 'Minnesota',
    seed: 10,
    bracket: 'East',
  },
  {
    team_name: 'Michigan State',
    seed: 2,
    bracket: 'East',
  },
  {
    team_name: 'Bradley',
    seed: 15,
    bracket: 'East',
  },
  {
    team_name: 'Gonzaga',
    seed: 1,
    bracket: 'West',
  },
  {
    team_name: 'FDickenson/PrView',
    seed: 16,
    bracket: 'West',
  },
  {
    team_name: 'Syracuse',
    seed: 8,
    bracket: 'West',
  },
  {
    team_name: 'Baylor',
    seed: 9,
    bracket: 'West',
  },
  {
    team_name: 'Marquette',
    seed: 5,
    bracket: 'West',
  },
  {
    team_name: 'Murray State',
    seed: 12,
    bracket: 'West',
  },
  {
    team_name: 'Florida State',
    seed: 4,
    bracket: 'West',
  },
  {
    team_name: 'Vermont',
    seed: 13,
    bracket: 'West',
  },
  {
    team_name: 'Buffalo',
    seed: 6,
    bracket: 'West',
  },
  {
    team_name: 'ASU/StJohns',
    seed: 11,
    bracket: 'West',
  },
  {
    team_name: 'Texas Tech',
    seed: 3,
    bracket: 'West',
  },
  {
    team_name: 'Northern Kentucky',
    seed: 14,
    bracket: 'West',
  },
  {
    team_name: 'Nevada',
    seed: 7,
    bracket: 'West',
  },
  {
    team_name: 'Florida',
    seed: 10,
    bracket: 'West',
  },
  {
    team_name: 'Michigan',
    seed: 2,
    bracket: 'West',
  },
  {
    team_name: 'Montana',
    seed: 15,
    bracket: 'West',
  },
  {
    team_name: 'Virginia',
    seed: 1,
    bracket: 'South',
  },
  {
    team_name: 'Gardner-Webb',
    seed: 16,
    bracket: 'South',
  },
  {
    team_name: 'Ole Miss',
    seed: 8,
    bracket: 'South',
  },
  {
    team_name: 'Oklahoma',
    seed: 9,
    bracket: 'South',
  },
  {
    team_name: 'Wisconsin',
    seed: 5,
    bracket: 'South',
  },
  {
    team_name: 'Oregon',
    seed: 12,
    bracket: 'South',
  },
  {
    team_name: 'Kansas State',
    seed: 4,
    bracket: 'South',
  },
  {
    team_name: 'UC Irvine',
    seed: 13,
    bracket: 'South',
  },
  {
    team_name: 'Villanova',
    seed: 6,
    bracket: 'South',
  },
  {
    team_name: 'Saint Mary',
    seed: 11,
    bracket: 'South',
  },
  {
    team_name: 'Purdue',
    seed: 3,
    bracket: 'South',
  },
  {
    team_name: 'Old Dominion',
    seed: 14,
    bracket: 'South',
  },
  {
    team_name: 'Cincinnati',
    seed: 7,
    bracket: 'South',
  },
  {
    team_name: 'Iowa',
    seed: 10,
    bracket: 'South',
  },
  {
    team_name: 'Tennessee',
    seed: 2,
    bracket: 'South',
  },
  {
    team_name: 'Colgate',
    seed: 15,
    bracket: 'South',
  },
  {
    team_name: 'North Carolina',
    seed: 1,
    bracket: 'Midwest',
  },
  {
    team_name: 'Iona',
    seed: 16,
    bracket: 'Midwest',
  },
  {
    team_name: 'Utah State',
    seed: 8,
    bracket: 'Midwest',
  },
  {
    team_name: 'Washington',
    seed: 9,
    bracket: 'Midwest',
  },
  {
    team_name: 'Auburn',
    seed: 5,
    bracket: 'Midwest',
  },
  {
    team_name: 'New Mexico State',
    seed: 12,
    bracket: 'Midwest',
  },
  {
    team_name: 'Kansas',
    seed: 4,
    bracket: 'Midwest',
  },
  {
    team_name: 'Northeastern',
    seed: 13,
    bracket: 'Midwest',
  },
  {
    team_name: 'Iowa State',
    seed: 6,
    bracket: 'Midwest',
  },
  {
    team_name: 'Ohio State',
    seed: 11,
    bracket: 'Midwest',
  },
  {
    team_name: 'Houston',
    seed: 3,
    bracket: 'Midwest',
  },
  {
    team_name: 'Georgia State',
    seed: 14,
    bracket: 'Midwest',
  },
  {
    team_name: 'Wofford',
    seed: 7,
    bracket: 'Midwest',
  },
  {
    team_name: 'Seton Hall',
    seed: 10,
    bracket: 'Midwest',
  },
  {
    team_name: 'Kentucky',
    seed: 2,
    bracket: 'Midwest',
  },
  {
    team_name: 'Abilene Christian',
    seed: 15,
    bracket: 'Midwest',
  },
];

const seedGamesData = [
  {
    "id" : "e-64-1",
    "seeds" : "e-32-1:away",
  },
  {
    "id" : "e-64-2",
    "seeds" : "e-32-1:home",
  },
  {
    "id" : "e-64-3",
    "seeds" : "e-32-2:away",
  },
  {
    "id" : "e-64-4",
    "seeds" : "e-32-2:home",
  },
  {
    "id" : "e-64-5",
    "seeds" : "e-32-3:away",
  },
  {
    "id" : "e-64-6",
    "seeds" : "e-32-3:home",
  },
  {
    "id" : "e-64-7",
    "seeds" : "e-32-4:away",
  },
  {
    "id" : "e-64-8",
    "seeds" : "e-32-4:home",
  },
  {
    "id" : "m-64-1",
    "seeds" : "m-32-1:away",
  },
  {
    "id" : "m-64-2",
    "seeds" : "m-32-1:home",
  },
  {
    "id" : "m-64-3",
    "seeds" : "m-32-2:away",
  },
  {
    "id" : "m-64-4",
    "seeds" : "m-32-2:home",
  },
  {
    "id" : "m-64-5",
    "seeds" : "m-32-3:away",
  },
  {
    "id" : "m-64-6",
    "seeds" : "m-32-3:home",
  },
  {
    "id" : "m-64-7",
    "seeds" : "m-32-4:away",
  },
  {
    "id" : "m-64-8",
    "seeds" : "m-32-4:home",
  },
  {
    "id" : "s-64-1",
    "seeds" : "s-32-1:away",
  },
  {
    "id" : "s-64-2",
    "seeds" : "s-32-1:home",
  },
  {
    "id" : "s-64-3",
    "seeds" : "s-32-2:away",
  },
  {
    "id" : "s-64-4",
    "seeds" : "s-32-2:home",
  },
  {
    "id" : "s-64-5",
    "seeds" : "s-32-3:away",
  },
  {
    "id" : "s-64-6",
    "seeds" : "s-32-3:home",
  },
  {
    "id" : "s-64-7",
    "seeds" : "s-32-4:away",
  },
  {
    "id" : "s-64-8",
    "seeds" : "s-32-4:home",
  },
  {
    "id" : "w-64-1",
    "seeds" : "w-32-1:away",
  },
  {
    "id" : "w-64-2",
    "seeds" : "w-32-1:home",
  },
  {
    "id" : "w-64-3",
    "seeds" : "w-32-2:away",
  },
  {
    "id" : "w-64-4",
    "seeds" : "w-32-2:home",
  },
  {
    "id" : "w-64-5",
    "seeds" : "w-32-3:away",
  },
  {
    "id" : "w-64-6",
    "seeds" : "w-32-3:home",
  },
  {
    "id" : "w-64-7",
    "seeds" : "w-32-4:away",
  },
  {
    "id" : "w-64-8",
    "seeds" : "w-32-4:home",
  },
  {
    "id" : "e-32-1",
    "seeds" : "e-16-1:away",
  },
  {
    "id" : "e-32-2",
    "seeds" : "e-16-1:home",
  },
  {
    "id" : "e-32-3",
    "seeds" : "e-16-2:home",
  },
  {
    "id" : "e-32-4",
    "seeds" : "e-16-2:away",
  },
  {
    "id" : "m-32-1",
    "seeds" : "m-16-1:away",
  },
  {
    "id" : "m-32-2",
    "seeds" : "m-16-1:home",
  },
  {
    "id" : "m-32-3",
    "seeds" : "m-16-2:away",
  },
  {
    "id" : "m-32-4",
    "seeds" : "m-16-2:home",
  },
  {
    "id" : "s-32-1",
    "seeds" : "s-16-1:away",
  },
  {
    "id" : "s-32-2",
    "seeds" : "s-16-1:home",
  },
  {
    "id" : "s-32-3",
    "seeds" : "s-16-2:away",
  },
  {
    "id" : "s-32-4",
    "seeds" : "s-16-2:home",
  },
  {
    "id" : "w-32-1",
    "seeds" : "w-16-1:away",
  },
  {
    "id" : "w-32-2",
    "seeds" : "w-16-1:home",
  },
  {
    "id" : "w-32-3",
    "seeds" : "w-16-2:away",
  },
  {
    "id" : "w-32-4",
    "seeds" : "w-16-2:home",
  },
  {
    "id" : "e-16-1",
    "seeds" : "e-8-1:away",
  },
  {
    "id" : "e-16-2",
    "seeds" : "e-8-1:home",
  },
  {
    "id" : "m-16-1",
    "seeds" : "m-8-1:away",
  },
  {
    "id" : "m-16-2",
    "seeds" : "m-8-1:home",
  },
  {
    "id" : "s-16-1",
    "seeds" : "s-8-1:away",
  },
  {
    "id" : "s-16-2",
    "seeds" : "s-8-1:home",
  },
  {
    "id" : "w-16-1",
    "seeds" : "w-8-1:away",
  },
  {
    "id" : "w-16-2",
    "seeds" : "w-8-1:home",
  },
  {
    "id" : "e-8-1",
    "seeds" : "f-4-2:away",
  },
  {
    "id" : "s-8-1",
    "seeds" : "f-4-1:away",
  },
  {
    "id" : "w-8-1",
    "seeds" : "f-4-1:home",
  },
  {
    "id" : "m-8-1",
    "seeds" : "f-4-2:home",
  },
  {
    "id" : "f-4-1",
    "seeds" : "f-2-1:away",
  },
  {
    "id" : "f-4-2",
    "seeds" : "f-2-1:home",
  },
  {
    "id" : "f-2-1",
    "seeds" : "f-1-1:home",
  },
  {
    "id" : "f-1-1",
    "seeds" : "champion",
  },
];

export const seedTeams = () => {
  return seedTeamsData.map(t => ({ name: t.name, short_name: t.short_name }));
};

export const seedCompetitors = async () => {
  return await Promise.all(seedCompetitorsData.map(async t => {
    const team  = await teams.find({ where: { name: t.team_name}}) as {id: number};
    if (!team) {
      throw new Error(`cannot find team ${t.team_name}!`);
    }

    return {
      team_id: team.id,
      bracket: t.bracket,
      seed: t.seed,
    };
  }));
};

export const seedGames = async () => {
  const brackets: {[key: string]: string} = { w: 'West', s: 'South', m: 'Midwest', e: 'East' };
  const seedsByGame: {[key: string]: { away: number, home: number }} = {
    1: { away: 1, home: 16 },
    2: { away: 8, home: 9  },
    3: { away: 5, home: 12 },
    4: { away: 4, home: 13 },
    5: { away: 6, home: 11 },
    6: { away: 3, home: 14 },
    7: { away: 7, home: 10 },
    8: { away: 2, home: 15 },
  };
  const nextHomeOrAway = (gameId: number) => gameId % 2 === 0 ? 'home' : 'away';
  const nextGameId = (gameId: number) => Math.floor((gameId-1) / 2) + 1;
  return await Promise.all(seedGamesData.map(async g => {
    const [bracket, round, game] = g.id.split('-');
    if (round !== '64') {
      return {
        id: g.id,
        seeds: g.seeds,
      };
    }
    const away_competitor = await competitors.find(
      { where: { bracket: brackets[bracket], seed: seedsByGame[game].away } }
    ) as {id: number};
    const home_competitor = await competitors.find(
      { where: { bracket: brackets[bracket], seed: seedsByGame[game].home} }
    ) as {id: number};

    if (!away_competitor || !home_competitor) {
      throw new Error(`cannot find competitor in bracket ${bracket} and game ${game}`);
    }

    const gameId = parseInt(game, 10);

    return {
      id: g.id,
      seeds: `${bracket}-32-${nextGameId(gameId)}:${nextHomeOrAway(gameId)}`,
      away_team: away_competitor.id,
      home_team: home_competitor.id,
    };
  }));
};

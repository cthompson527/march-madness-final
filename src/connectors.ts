import database from './database';
import { seedCompetitors, seedGames, seedTeams } from './seeders/initialData';
import { postUser } from './handlers/userHandler';

export const teams = database.import('./models/team');
export const competitors = database.import('./models/competitor');
export const games = database.import('./models/game');
export const user = database.import('./models/user');
export const credential = database.import('./models/credential');
export const selections = database.import('./models/selection');

export const setupDb = async () => {
  await database.sync();

  if ((await teams.count()) === 0) {
    await database.getQueryInterface().bulkInsert('teams', seedTeams());

    const competitors = await seedCompetitors();
    await database.getQueryInterface().bulkInsert('competitors', competitors);

    const games = await seedGames();
    await database.getQueryInterface().bulkInsert('games', games);

    await postUser(
      {
        body: {
          email: 'a@a.com',
          first_name: 'Cory',
          last_name: 'Thompson',
          password: 'password',
          admin: true,
        },
      } as any,
      {
        send: (msg: string) => console.log(msg),
      } as any,
    );

    await database.query(`
      CREATE VIEW winners AS 
        SELECT id, away_team AS winner FROM games WHERE away_score > home_score 
          UNION ALL 
        SELECT id, home_team AS winner FROM games WHERE home_score > away_score;
    `);
  }
};

credential.belongsTo(user, { foreignKey: 'user_id' });
user.hasOne(credential, { foreignKey: 'user_id' });

games.belongsTo(competitors, { foreignKey: 'away_team', as: 'away_competitor' });
games.belongsTo(competitors, { foreignKey: 'home_team', as: 'home_competitor' });

competitors.belongsTo(teams, { foreignKey: 'team_id' });
teams.hasOne(competitors, { foreignKey: 'team_id' });

selections.belongsTo(user, { foreignKey: 'user_id' });
selections.belongsTo(games, { foreignKey: 'game_id' });
selections.belongsTo(competitors, { foreignKey: 'competitor_id' });
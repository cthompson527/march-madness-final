import { Request, Response } from 'express';
import { teams } from '../connectors';

export const getTeams = async (_: Request, res: Response) => {
  const t = await teams.findAll();
  res.send(t);
};

import { Request, Response } from 'express';
import database from '../database';
import { selections, user } from '../connectors';

interface Selection {
  user_id: string;
  competitor_id: number;
  game_id: string;

  user: {
    email: string;
    first_name: string;
    last_name: string;
  };
}

interface UserScore {
  user_id: string;
  email: string;
  first_name: string;
  last_name: string;
  score: number;
}

const fibonacciScores: { [round: number]: number } = {
  64: 2,
  32: 3,
  16: 5,
  8: 8,
  4: 13,
  2: 21,
};

export const getScores = async (_: Request, res: Response) => {
  const [winners] = (await database.query('SELECT * FROM winners;')) as [
    Array<{ id: string; winner: number }>
  ];
  const gamesById: { [gameId: string]: { id: string; winner: number } } = {};
  for (const winner of winners) {
    gamesById[winner.id] = winner;
  }
  const s = (await selections.findAll({ include: [user] })) as Array<Selection>;
  const scores: { [user_id: string]: UserScore } = {};

  for (const sel of s) {
    scores[sel.user_id] = {
      user_id: sel.user_id,
      email: sel.user.email,
      first_name: sel.user.first_name,
      last_name: sel.user.last_name,
      score: 0,
    };
  }

  for (const sel of s) {
    if (!gamesById[sel.game_id]) {
      continue;
    }
    if (sel.competitor_id === gamesById[sel.game_id].winner) {
      const round = parseInt(sel.game_id.split('-')[1], 10);
      scores[sel.user_id].score += fibonacciScores[round];
    }
  }

  res.send(scores);
};

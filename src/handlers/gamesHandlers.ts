import { Request, Response } from 'express';
import { competitors, games, teams } from '../connectors';

interface Game {
  id: string;
  seeds: string;
  away_team: number;
  home_team: number;
  away_score: number;
  home_score: number;
}

export const getGames = async (_: Request, res: Response) => {
  const g = await games.findAll({
    include: [
      { model: competitors, include: [teams], as: 'away_competitor' },
      { model: competitors, include: [teams], as: 'home_competitor' },
    ],
  });
  res.send(g);
};

export const postGames = async (req: Request, res: Response) => {
  if (req.headers.admin !== 'true') {
    res.status(401).send({ success: false, error: 'User must be an admin' });
    return;
  }
  const { games: gamesInput } = req.body as { games: Game[] };
  for (const game of gamesInput) {
    games.upsert({ ...game });
  }
  res.send({ success: true });
};

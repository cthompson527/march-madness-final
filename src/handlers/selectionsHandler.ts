import { Request, Response } from 'express';
import { selections as selectionsModel } from '../connectors';
import isAfter from 'date-fns/is_after';

export const postSelections = async (req: Request, res: Response) => {
  // is this after March 21 at midnight?
  if (isAfter(new Date(), new Date(2019, 2, 21, 17, 0, 0, 0))) {
    res
      .status(401)
      .send({ success: false, msg: 'no longer are submissions allowed' });
    return;
  }
  const { selections } = req.body;
  const { user_id } = req.headers;
  for (const selection of selections) {
    const { game_id, competitor_id } = selection;
    selectionsModel.upsert({ user_id, game_id, competitor_id });
  }
  res.send({ success: true });
};

export const getSelections = async (req: Request, res: Response) => {
  const user_id = req.params.userid || req.headers.user_id;
  const selections = await selectionsModel.findAll({ where: { user_id } });
  res.send(selections);
};

import { Request, Response } from 'express';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { credential, user } from '../connectors';

const salt = bcrypt.genSaltSync(10);
const JWT_SECRET = process.env.JWT_SECRET || 'thisisasecret';

export const generatePassword = (password: string) =>
  bcrypt.hashSync(password, salt);

export const postUser = async (req: Request, res: Response) => {
  const { email, first_name, last_name, password } = req.body;

  if (!(email && first_name && last_name && password)) {
    res.status(400).send(`missing all user information. can't create a user`);
    return;
  }

  const [newUser] = (await user.upsert(
    { email, first_name, last_name, password, admin: false },
    { returning: true },
  )) as [{ uuid: string }, boolean];

  const user_id = newUser.uuid;
  await credential.upsert({
    user_id,
    password: generatePassword(password),
  });

  res.send({ status: 'success!' });
};

export const login = async (req: Request, res: Response) => {
  const { email, password } = req.body;

  if (!(email && password)) {
    res.status(400).send(`missing all user information. can't create a user`);
    return;
  }

  try {
    const { credential: credTable, admin, uuid, first_name, last_name } = (await user.findOne({
      where: { email },
      include: [credential],
    })) as { uuid: string; admin: boolean; first_name: string; last_name: string; credential?: { password: string } };

    if (bcrypt.compareSync(password, (credTable && credTable.password) || '')) {
      res.setHeader(
        'Authorization',
        jwt.sign({ uuid, admin, first_name, last_name }, JWT_SECRET, { expiresIn: '90d' }),
      );
      res.send({
        login: 'success',
      });
    } else {
      throw 'invalid username or password';
    }
  } catch (err) {
    res
      .status(401)
      .send({ login: 'failed', error: 'incorrect username or password' });
  }
};

export const verifyToken = (t: string) =>
  jwt.verify(t, JWT_SECRET) as { uuid: string, admin: boolean };

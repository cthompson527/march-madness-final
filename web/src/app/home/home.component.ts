import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import {
  Competitor,
  Game,
  UserSelection,
} from 'src/app/bracket/bracket.component';
import { ActivatedRoute } from '@angular/router';
import hasStarted from 'src/app/utility/hasStarted';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  games: { [gameId: string]: Game };
  selections: { [gameId: string]: UserSelection };
  receivedData = 0;
  brackets = [
    { value: 's', viewValue: 'South' },
    { value: 'w', viewValue: 'West' },
    { value: 'e', viewValue: 'East' },
    { value: 'm', viewValue: 'Midwest' },
    { value: 'f', viewValue: 'Final Four' },
  ];
  bracketId = this.brackets[0].value;
  getParentIdOf: { [gameId: string]: string } = {};
  competitors: { [compId: string]: Competitor } = {};

  constructor(private apiService: ApiService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.apiService.getGames().subscribe((g: Game[]) => {
      const games = {};
      for (const game of g) {
        games[game.id] = game;
        this.getParentIdOf[game.seeds] = game.id;
        this.competitors[game.away_team] = game.away_competitor;
        this.competitors[game.home_team] = game.home_competitor;
      }
      this.games = games;
      this.receivedData += 1;
    });

    const userId = this.route.snapshot.paramMap.get('userid');
    this.apiService.getSelections(userId).subscribe((s: UserSelection[]) => {
      const selections = {};
      for (const selection of s) {
        selections[selection.game_id] = selection;
      }
      this.selections = selections;
      this.receivedData += 1;
    });
  }

  displaySeed(gameId: string, homeAway: string) {
    const game = this.getGame(gameId);
    if (!game) {
      return '';
    }
    const competitor = game[`${homeAway}_competitor`] as Competitor;
    if (!competitor) {
      const parentId = this.getParentIdOf[`${gameId}:${homeAway}`];
      const selectedGame = this.selections[parentId];
      if (!selectedGame) {
        return '';
      }
      return this.competitors[selectedGame.competitor_id].seed;
    }
    return competitor.seed;
  }

  buildBracketWinnerId() {
    return `f-4-${this.bracketId === 's' || this.bracketId === 'w' ? 1 : 2}`;
  }

  getBracketName() {
    return this.brackets.find(b => b.value === this.bracketId).viewValue;
  }

  getGame(gameId: string) {
    return this.games[gameId];
  }

  displayTeam(gameId: string, homeAway: string) {
    const game = this.getGame(gameId);
    if (!game) {
      return '';
    }
    const competitor = game[`${homeAway}_competitor`] as Competitor;
    if (!competitor) {
      const parentId = this.getParentIdOf[`${gameId}:${homeAway}`];
      const selectedGame = this.selections[parentId];
      if (!selectedGame) {
        return '';
      }
      return this.competitors[selectedGame.competitor_id].team.name;
    }
    return competitor.team.name;
  }

  buildGameId(round: number, gameId: number) {
    return `${this.bracketId}-${round}-${gameId}`;
  }

  isPickCorrect(gameId: string, homeAway: string) {
    const game = this.getGame(gameId);
    const selectedGame = this.selections[
      this.getParentIdOf[`${gameId}:${homeAway}`]
    ];
    if (!selectedGame) {
      return '';
    }

    if (game[`${homeAway}_team`] === null) {
      return 'not-played';
    }

    if (game[`${homeAway}_team`] !== selectedGame.competitor_id) {
      return 'wrong';
    }

    return '';
  }

  popover(gameId: string, homeAway: string) {
    const selectedGame = this.selections[
      this.getParentIdOf[`${gameId}:${homeAway}`]
    ];
    if (!selectedGame) {
      return `You haven't made any selections yet.`;
    }
    return `You selected: ${
      this.competitors[selectedGame.competitor_id].team.name
    }`;
  }
  
  hasTournamentStarted() {
    return hasStarted();
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../environments/environment';
import { AuthService } from './auth/auth.service';
import { Game } from 'src/app/bracket/bracket.component';
import { User } from 'src/app/register/register.component';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(
    public http: HttpClient,
    private router: Router,
    private authService: AuthService,
  ) {}

  getHeaders() {
    const token = localStorage.getItem('token');
    return {
      Authorization: `Bearer ${token}`,
    };
  }

  login(values, successCb, errorCb) {
    this.http
      .post(`${environment.apiURL}/login`, values, {
        observe: 'response',
        responseType: 'json',
      })
      .subscribe(
        response => {
          const token = response.headers.get('Authorization');
          localStorage.setItem('token', token);
          this.authService.isLoggedInValue = true;
          successCb();
        },
        err => {
          localStorage.removeItem('token');
          this.authService.isLoggedInValue = false;
          errorCb();
        },
      );
  }

  getGames() {
    return this.http.get(`${environment.apiURL}/games`, {
      headers: this.getHeaders(),
    });
  }

  postGames(games: Game[]) {
    return this.http.post(
      `${environment.apiURL}/games`,
      { games },
      { headers: this.getHeaders() },
    );
  }

  getSelections(userId?: string) {
    if (userId) {
      return this.http.get(`${environment.apiURL}/selections/${userId}`, {
        headers: this.getHeaders(),
      });
    }
    return this.http.get(`${environment.apiURL}/selections/`, {
      headers: this.getHeaders(),
    });
  }

  postSelections(selections: { game_id: string; competitor_id: number }[]) {
    return this.http.post(
      `${environment.apiURL}/selections`,
      { selections },
      { headers: this.getHeaders() },
    );
  }

  getScores() {
    return this.http.get(`${environment.apiURL}/scores`, {
      headers: this.getHeaders(),
    });
  }

  postUser(user: User) {
    return this.http.post(`${environment.apiURL}/register`, user, {
      headers: this.getHeaders(),
    });
  }
}

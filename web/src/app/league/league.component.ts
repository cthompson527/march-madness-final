import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import hasStarted from 'src/app/utility/hasStarted';

interface UserScore {
  user_id: string;
  email: string;
  first_name: string;
  last_name: string;
  score: number;
}

@Component({
  selector: 'app-league',
  templateUrl: './league.component.html',
  styleUrls: ['./league.component.scss']
})
export class LeagueComponent implements OnInit {
  scores: UserScore[];
  displayedColumns: string[] = ['First Name', 'Last Name', 'Score'];

  constructor(private apiService: ApiService) { }

  getRouterLink(userId: string) {
    if (hasStarted()) {
      return `/bracket/view/${userId}`;
    }
  }

  hasTournamentStarted() {
    return hasStarted();
  }

  ngOnInit() {
    this.apiService.getScores().subscribe((scores: { [userId: string]: UserScore }) => {
      this.scores = Object.keys(scores).map(userId => scores[userId]).sort((s1, s2) => s2.score - s1.score);
    });
  }
}

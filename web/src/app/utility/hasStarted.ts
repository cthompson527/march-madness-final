import * as isAfter from 'date-fns/is_after';

export default () => {
  // is this after March 21 at midnight?
  return isAfter(new Date(), new Date(2019, 2, 21, 12, 0, 0, 0));
}

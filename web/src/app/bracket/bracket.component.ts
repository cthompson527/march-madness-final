import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

export interface Game {
  id:               string;
  seeds:            string;
  away_team?:       number;
  home_team?:       number;
  away_score?:      number;
  home_score?:      number;
  away_competitor?: Competitor;
  home_competitor?: Competitor;
}

export interface Competitor {
  id:      number;
  team_id: number;
  bracket: Bracket;
  seed:    number;
  team:    Team;
}

export enum Bracket {
  East = "East",
  Midwest = "Midwest",
  South = "South",
  West = "West",
}

export interface Team {
  id:         number;
  name:       string;
  short_name: null | string;
}

export interface UserSelection {
  game_id: string;
  competitor_id: number;
}

@Component({
  selector: 'app-bracket',
  templateUrl: './bracket.component.html',
  styleUrls: ['./bracket.component.scss']
})
export class BracketComponent implements OnInit {
  games: Game[];
  gamesById: {[gameId: string]: Game} = {};
  receivedData: boolean = false;
  hoverTeam: number | null;
  brackets = [
    { value: 's', viewValue: 'South', },
    { value: 'w', viewValue: 'West' },
    { value: 'e', viewValue: 'East', },
    { value: 'm', viewValue: 'Midwest' },
    { value: 'f', viewValue: 'Final Four' },
  ];
  bracketId = this.brackets[0].value;

  constructor(private apiService: ApiService, private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.apiService.getGames().subscribe(
      (games: Game[]) => {
        this.games = games;
        this.games.map(g => {
          this.gamesById[g.id] = g
        });

        this.apiService.getSelections().subscribe(
          (selections: UserSelection[]) => {
            // updateTable
            selections.sort((a, b) => {
              const aRound = a.game_id.split('-')[1];
              const bRound = b.game_id.split('-')[1];
              return (-(parseInt(aRound, 10) - parseInt(bRound, 10)));
            });
            for (const selection of selections) {
              const { competitor_id, game_id } = selection;
              const { seeds } = this.gamesById[game_id];
              const [ seedsGameId, seedsHomeAway ] = seeds.split(':');
              let competitor;
              if (this.gamesById[game_id].away_team === competitor_id) {
                competitor = this.gamesById[game_id].away_competitor;
              } else {
                competitor = this.gamesById[game_id].home_competitor;
              }
              this.gamesById[seedsGameId][`${seedsHomeAway}_competitor`] = competitor;
              this.gamesById[seedsGameId][`${seedsHomeAway}_team`] = competitor_id;
            }
          }
        );
        this.receivedData = true;
      });
  }

  buildGameId(round: number, gameId: number) {
    return `${this.bracketId}-${round}-${gameId}`;
  }

  buildBracketWinnerId() {
    return `f-4-${this.bracketId === 's' || this.bracketId === 'w' ? 1 : 2}`;
  }

  getBracketName() {
    return this.brackets.find(b => b.value === this.bracketId).viewValue;
  }

  getGameById(gameId: string) {
    return this.gamesById[gameId];
  }

  displayTeam(gameId: string, homeAway: string) {
    const game = this.getGameById(gameId);
    const competitor = game[`${homeAway}_competitor`] as Competitor;
    if (!competitor) {
      return '';
    }
    return competitor.team.name;
  }

  displaySeed(gameId: string, homeAway: string) {
    const game = this.getGameById(gameId);
    const competitor = game[`${homeAway}_competitor`] as Competitor;
    if (!competitor) {
      return '';
    }
    return competitor.seed;
  }

  displayBracketName() {
    return this.brackets.find(b => b.value === this.bracketId).viewValue;
  }

  getOpposite(homeAway: string) {
    return homeAway === 'home' ? 'away' : 'home';
  }

  checkFutureSeed(gameId: string, oldCompetitor: number, homeAway: string) {
    const game = this.gamesById[gameId];
    if (!game) {
      return;
    }

    const competitor = game[`${homeAway}_competitor`];

    if (!competitor) {
      return;
    }

    if (competitor.id === oldCompetitor) {
      game[`${homeAway}_competitor`] = null;
      const [nextGameId, nextGameTeam] = game.seeds.split(':');
      this.checkFutureSeed(nextGameId, oldCompetitor, nextGameTeam);
    }
  }

  handleSelection(game: Game, homeAway: string) {
    const [nextGameId, nextGameTeam] = game.seeds.split(':');
    const [_, nextRound, nextGameGameID] = nextGameId.split('-');
    const opposite = this.getOpposite(homeAway);
    this.checkFutureSeed(nextGameId, this.gamesById[game.id][`${opposite}_team`], nextGameTeam);
    if (nextRound === '8') {
      const nextSeedsRound = 4;
      const nextSeedsGame = (this.bracketId === 's' || this.bracketId === 'w') ? 1 : 2;
      const nextSeedsTeam = (this.bracketId === 's' || this.bracketId === 'e') ? 'away' : 'home';
      this.gamesById[nextGameId] = {
        ...this.gamesById[nextGameId],
        id: nextGameId,
        seeds: `f-${nextSeedsRound}-${nextSeedsGame}:${nextSeedsTeam}`,
        [`${nextGameTeam}_competitor`]: game[`${homeAway}_competitor`]
      };
      return;
    }

    if (nextRound === '4') {
      const nextSeedsRound = 2;
      const nextSeedsGame = 1;
      const nextSeedsTeam = (this.bracketId === 's' || this.bracketId === 'w') ? 'away' : 'home';
      this.gamesById[nextGameId] = {
        ...this.gamesById[nextGameId],
        id: nextGameId,
        seeds: `f-${nextSeedsRound}-${nextSeedsGame}:${nextSeedsTeam}`,
        [`${nextGameTeam}_competitor`]: game[`${homeAway}_competitor`]
      };
      return;
    }

    if (nextRound === '2') {
      const nextSeedsRound = 1;
      const nextSeedsGame = 1;
      const nextSeedsTeam = 'home';
      this.gamesById[nextGameId] = {
        ...this.gamesById[nextGameId],
        id: nextGameId,
        seeds: `f-${nextSeedsRound}-${nextSeedsGame}:${nextSeedsTeam}`,
        [`${nextGameTeam}_competitor`]: game[`${homeAway}_competitor`]
      };
      return;
    }

    if (nextRound === '1') {
      const seeds = 'champion';
      this.gamesById[nextGameId] = {
        id: nextGameId,
        seeds,
        home_competitor: game[`${homeAway}_competitor`]
      };
      return;
    }

    const nextSeedsRound = parseInt(nextRound, 10) / 2;
    const nextSeedsGame = Math.ceil(parseInt(nextGameGameID, 10) / 2);
    const nextSeedsTeam = parseInt(nextGameGameID, 10) % 2 === 0 ? 'home' : 'away';
    this.gamesById[nextGameId] = {
      ...this.gamesById[nextGameId],
      id: nextGameId,
      seeds: `${this.bracketId}-${nextSeedsRound}-${nextSeedsGame}:${nextSeedsTeam}`,
      [`${nextGameTeam}_competitor`]: game[`${homeAway}_competitor`]
    };
  }

  highlightTeam(game: Game, homeAway: string) {
    if (!game || !game[`${homeAway}_competitor`]) {
      return;
    }
    const teamToHighlight = game[`${homeAway}_competitor`] as Competitor;
    this.hoverTeam = teamToHighlight.id;
  }

  isHighlighted(game: Game, homeAway: string) {
    if (!game || !this.hoverTeam) {
      return false;
    }
    const teamToHighlight = game[`${homeAway}_competitor`] as Competitor;
    return teamToHighlight && this.hoverTeam === teamToHighlight.id;
  }

  resetHighlight() {
    this.hoverTeam = null;
  }

  cancelSelections() {
    this.router.navigateByUrl('bracket');
  }

  sendSelections() {
    const selections = Object.values(this.gamesById)
      .filter(v => {
        const [ gameId, homeAway ] = v.seeds.split(':');
        const gameIdExists = this.gamesById.hasOwnProperty(gameId);
        if (!gameIdExists) {
          return false;
        }
        return !!this.gamesById[gameId][`${homeAway}_competitor`];
      })
      .map(v => {
        const { seeds } = v;
        const [ gameId, homeAway ] = seeds.split(':');
        const competitor_id = this.gamesById[gameId][`${homeAway}_competitor`].id;

        return { game_id: v.id, competitor_id };
      }
    );

    this.apiService.postSelections(selections)
      .subscribe(res => {
        this.router.navigateByUrl('bracket');
        this.openSnackBar();
      }, err => err.status === 200 ? '' : console.error(err));
  }

  openSnackBar() {
    this.snackBar.openFromComponent(SaveBracketToastComponent, {
      duration: 2000,
    });
  }
}

@Component({
  selector: 'snack-bar-component-save-bracket',
  template: `
    <span class="example-pizza-party">
      Bracket Saved! 💾
    </span>`,
  styles: [`
    .example-pizza-party {
      color: hotpink;
    }
  `],
})
export class SaveBracketToastComponent {}


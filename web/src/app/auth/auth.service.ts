import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Subject } from 'rxjs';

interface User {
  email: string;
  admin: boolean;
  first_name: string;
  last_name: string;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isLoggedInValue = false;

  user: User = null;

  fullName = new Subject<string>();
  adminSubject = new Subject<boolean>();
  administrator = false;

  isLoggedIn() {
    const token = localStorage.getItem('token');
    if (!token) {
      this.fullName.next('');
      this.isLoggedInValue = false;
      return this.isLoggedInValue;
    }

    const helper = new JwtHelperService();

    if (helper.isTokenExpired(token)) {
      this.isLoggedInValue = false;
      return this.isLoggedInValue;
    }

    const decodedToken = helper.decodeToken(token) as User;

    this.user = { email: decodedToken.email, admin: decodedToken.admin, first_name: decodedToken.first_name, last_name: decodedToken.last_name };

    this.fullName.next(`${this.user.first_name} ${this.user.last_name}`);
    this.administrator = this.user.admin;
    this.adminSubject.next(this.user.admin);

    this.isLoggedInValue = true;
    return this.isLoggedInValue;
  }

  getName() {
    return this.fullName;
  }

  getAdmin() {
    return this.adminSubject;
  }

  logOut() {
    localStorage.removeItem('token');
    this.fullName.next('');
    this.administrator = false;
    this.adminSubject.next(false);
  }

  constructor() {}
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './guards/auth.guard';
import { EditGuard } from './guards/edit.guard';
import { LogoutComponent } from './logout/logout.component';
import { BracketComponent } from './bracket/bracket.component';
import { LeagueComponent } from './league/league.component';
import { AdminComponent } from './admin/admin.component';
import { RegisterComponent } from 'src/app/register/register.component';
import { AdminGuard } from 'src/app/guards/admin.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'logout', component: LogoutComponent },
  {
    path: 'bracket',
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'bracket/view/:userid',
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'bracket/edit',
    component: BracketComponent,
    canActivate: [AuthGuard, EditGuard],
  },
  {
    path: 'league',
    component: LeagueComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard, AdminGuard],
  },
  {
    path: '**',
    redirectTo: 'bracket',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

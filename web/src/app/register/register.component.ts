import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators
} from '@angular/forms';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';

export interface User {
  email: string;
  first_name: string;
  last_name: string;
  password: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required]],
    }, { validators: RegisterComponent.MatchPassword });
  }

  static MatchPassword(control: AbstractControl) {
    const password = control.get('password');
    const confirmPassword = control.get('confirmPassword');
    const passwordMatch = password.value === confirmPassword.value;
    return passwordMatch ? null : confirmPassword.setErrors( { MatchPassword: true });
  }

  createUser() {
    const first_name = this.firstName.value;
    const last_name = this.lastName.value;
    const email = this.email.value;
    const password = this.password.value;

    this.apiService.postUser({ first_name, last_name, email, password }).subscribe(res => this.router.navigateByUrl('login'));
  }

  cancel() {
    this.router.navigateByUrl('login');
  }

  get firstName() {
    return this.registerForm.get('firstName');
  }

  get lastName() {
    return this.registerForm.get('lastName');
  }

  get email() {
    return this.registerForm.get('email');
  }

  get password() {
    return this.registerForm.get('password');
  }

  get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  }

}


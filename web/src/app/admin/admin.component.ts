import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { Game } from 'src/app/bracket/bracket.component';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  roundId: string = '64';
  rounds = [
    { value: '64', viewLong: 'Round of 64', viewShort: '64' },
    { value: '32', viewLong: 'Round of 32', viewShort: '32' },
    { value: '16', viewLong: 'Sweet 16', viewShort: 'Sixteen' },
    { value: '8', viewLong: 'Elite 8', viewShort: 'Eight' },
    { value: '4', viewLong: 'Final Four', viewShort: 'Four' },
    { value: '2', viewLong: 'Championship', viewShort: 'Champ' },
  ];
  games: Game[];
  gamesSaved: Game[];
  dirtyGameIds: string[] = [];

  constructor(private apiService: ApiService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.apiService.getGames().subscribe((g: Game[]) => {
      this.games = g.slice();
      this.gamesSaved = this.games.map(g => ({ ...g }));
    });
  }

  getRoundName(longShort: string) {
    return this.rounds.find(r => r.value === this.roundId)[longShort];
  }

  getGames(b: string) {
    if (!this.games) {
      return [];
    }
    return this.games.filter(g => {
      const [bracket, round] = g.id.split('-');
      return this.roundId === round && b === bracket;
    })
      .sort((g1, g2) => {
        const num1 = parseInt(g1.id.split('-')[2], 10);
        const num2 = parseInt(g2.id.split('-')[2], 10);
        return num1 - num2;
      });
  }

  getTeam(game: Game, homeAway: string) {
    if (!game[`${homeAway}_competitor`]) {
      return '';
    }

    return game[`${homeAway}_competitor`].team.name;
  }

  handleScoreUpdate(game: Game, homeAway: string, event: Event) {
    const { value } = event.target as HTMLInputElement;
    game[`${homeAway}_score`] = parseInt(value, 10) || null;
    const savedGame = this.gamesSaved.find(g => g.id === game.id);

    if (
      game.home_score === savedGame.home_score &&
      game.away_score === savedGame.away_score
    ) {
      const indexOfGameId = this.dirtyGameIds.indexOf(game.id);
      if (indexOfGameId !== -1) {
        this.dirtyGameIds.splice(indexOfGameId, 1);
      }
    } else {
      const indexOfGameId = this.dirtyGameIds.indexOf(game.id);
      if (indexOfGameId === -1) {
        this.dirtyGameIds.push(game.id);
      }
    }

    const [ seedsGameId, seedsHomeAway ] = game.seeds.split(':');
    const seedsGame = this.games.find(g => g.id === seedsGameId);

    if (game.away_score && game.home_score) {
      const winner = game.away_score > game.home_score ? 'away' : 'home';
      seedsGame[`${seedsHomeAway}_team`] = game[`${winner}_team`];
    } else {
      seedsGame[`${seedsHomeAway}_team`] = null;
    }
  }

  isRowDirty(gameId: string) {
    return this.dirtyGameIds.indexOf(gameId) > -1;
  }

  resetGames() {
    this.games = this.gamesSaved.map(g => ({...g}));
    this.dirtyGameIds = [];
  }

  submitGames() {
    this.apiService.postGames(this.games).subscribe(g => {
      this.openSnackBar();
      this.getData();
      this.dirtyGameIds = [];
    });
  }

  openSnackBar() {
    this.snackBar.openFromComponent(SaveToastComponent, {
      duration: 2000,
    });
  }
}

@Component({
  selector: 'snack-bar-component-example-snack',
  template: `
    <span class="example-pizza-party">
      Games Saved! 💾
    </span>`,
  styles: [`
    .example-pizza-party {
      color: hotpink;
    }
  `],
})
export class SaveToastComponent {}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  fullName: string = '';
  isAdmin = false;

  constructor(private authService: AuthService) {}

  onLogin() {
    return window.location.pathname === '/login';
  }

  ngOnInit() {
    this.authService.getName().subscribe(name => this.fullName = name);
    this.authService.getAdmin().subscribe(admin => this.isAdmin = admin);
  }
}

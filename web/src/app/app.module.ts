import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material-module/material.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BracketComponent, SaveBracketToastComponent } from './bracket/bracket.component';
import { LeagueComponent } from './league/league.component';
import { AdminComponent, SaveToastComponent } from './admin/admin.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [AppComponent, LoginComponent, HomeComponent, NavbarComponent, BracketComponent, LeagueComponent, AdminComponent, LogoutComponent, SaveToastComponent, SaveBracketToastComponent, RegisterComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    NgxMatSelectSearchModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot(),
  ],
  entryComponents: [SaveToastComponent, SaveBracketToastComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
